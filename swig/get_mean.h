#ifdef __cplusplus
extern "C" {
#endif

/* Calculate the mean of a uniform distribution: */
void get_mean_(int* n, double* x, double* dx, double* mean);

/* Get the evolution of the mean for a uniform distribution: */
void get_mean_evol_(int* n, double* x, double* dx, double* v, int* m);

#ifdef __cplusplus
}
#endif
