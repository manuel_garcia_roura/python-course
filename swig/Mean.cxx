#include "Mean.hxx"

/* The MeanCalculator constructor: */
MeanCalculator::MeanCalculator(int n, double x, double dx) : n(n), x(x), dx(dx) {};

/* The MeanCalculator destructor: */
MeanCalculator::~MeanCalculator() {};

/* Set the calculation parameters: */
void MeanCalculator::setParams(int n0, double x0, double dx0) {
   
   /* Set the calculation parameters: */
   n = n0;
   x = x0;
   dx = x0;
   
};

/* Calculate the mean of a uniform distribution in C++: */
double MeanCalculator::getMeanCpp() {
   
   /* Initialize the random seed: */
   srand(time(NULL));
   
   /* Calculate the mean: */
   double mean = 0.0;
   for (int i = 0; i < n; i++)
      mean += x + (-1.0+2.0*float(rand())/RAND_MAX)*dx;
   mean /= n;
   
   /* Return the mean: */
   return mean;
   
};

/* Calculate the mean of a uniform distribution in Fortran90: */
double MeanCalculator::getMeanF90() {
   
   /* Just call the Fortran90 function: */
   double mean;
   get_mean_(&n, &x, &dx, &mean);
   
   /* Return the mean: */
   return mean;
   
};

/* Get the evolution of the mean for a uniform distribution in C++: */
void MeanCalculator::getMeanEvolCpp(double* v, int m) {
   
   /* Initialize the random seed: */
   srand(time(NULL));
   
   /* Calculate the mean: */
   double mean = 0.0;
   for (int i = 0; i < m; i++) {
      for (int j = 0; j < n/m; j++)
         mean += x + (-1.0+2.0*float(rand())/RAND_MAX)*dx;
      v[i] = mean / ((i+1)*n/m);
   }
   
};

/* Get the evolution of the mean for a uniform distribution in Fortran90: */
void MeanCalculator::getMeanEvolF90(double* v, int m) {
   
   /* Just call the Fortran90 function: */
   get_mean_evol_(&n, &x, &dx, v, &m);
   
};
