from Mean import MeanCalculator

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

import time as t
import random as r

# Calculate the mean of a uniform distribution in Python:
def getMeanPython(n, x, dx):
   
   # Initialize the random seed:
   r.seed()
   
   # Calculate the mean:
   mean = 0.0
   for i in range(n):
      mean += r.uniform(x-dx, x+dx)
   mean /= n
   
   # Return the mean:
   return mean

# Calculate the mean of a uniform distribution in NumPy:
def getMeanNumPy(n, x, dx):
   
   # Initialize the random seed:
   np.random.seed()
   
   # Calculate the mean:
   s = np.random.uniform(low = x-dx, high = x+dx, size = n)
   mean = np.mean(s)
   
   # Return the mean:
   return mean

# Get the evolution of the mean for a uniform distribution in Python:
def getMeanEvolPython(n, x, dx, m):
   
   # Initialize the random seed:
   r.seed()
   
   # Calculate the mean:
   v = np.empty(m)
   mean = 0.0
   for i in range(m):
      for j in range(n/m):
         mean += r.uniform(x-dx, x+dx)
      v[i] = mean / ((i+1)*n/m)
   
   # Return the mean evolution:
   return v

# Get the evolution of the mean for a uniform distribution in NumPy:
def getMeanEvolNumPy(n, x, dx, m):
   
   # Initialize the random seed:
   np.random.seed()
   
   # Calculate the mean:
   v = np.empty(m)
   s = np.random.uniform(low = x-dx, high = x+dx, size = n).reshape(m, n/m)
   mean = np.mean(s, axis = 1)
   for i in range(0, m):
      v[i] = np.mean(mean[0:i+1])
   
   # Return the mean evolution:
   return v

# Main program:
def main():
   
   # Parameters:
   n = 1000000
   x = 10.0
   dx = 10.0
   m = 20
   
   # Create a MeanCalculator object:
   mc = MeanCalculator(n, x, dx)
   
   # Calculate the mean in Python:
   tpy = t.time()
   xpy = getMeanPython(n, x, dx)
   vpy = getMeanEvolPython(n, x, dx, m)
   tpy = t.time() - tpy
   
   # Calculate the mean in NumPy:
   tnp = t.time()
   xnp = getMeanNumPy(n, x, dx)
   vnp = getMeanEvolNumPy(n, x, dx, m)
   tnp = t.time() - tnp
   
   # Calculate the mean in C++:
   tcpp = t.time()
   xcpp = mc.getMeanCpp()
   vcpp = mc.getMeanEvolCpp(m)
   tcpp = t.time() - tcpp
   
   # Calculate the mean in Fortran90:
   tf90 = t.time()
   xf90 = mc.getMeanF90()
   vf90 = mc.getMeanEvolF90(m)
   tf90 = t.time() - tf90
   
   # Recalculate the mean keeping the calculation time constant:
   n = int(tpy*n/tcpp)
   mc.setParams(n, x, dx)
   tcppref = t.time()
   xcppref = mc.getMeanCpp()
   vcppref = mc.getMeanEvolCpp(m)
   tcppref = t.time() - tcppref
   
   # Print the main results:
   print("Python: %f - %f" % (xpy, tpy))
   print("NumPy: %f - %f" % (xnp, tnp))
   print("C++: %f - %f" % (xcpp, tcpp))
   print("C++ (n = %d): %f - %f" % (n, xcppref, tcppref))
   print("Fortran90: %f - %f" % (xf90, tf90))
   
   # Plot the evolution for all the methods
   plt.plot(vpy, "-bo", label = "Python")
   plt.plot(vnp, "-go", label = "NumPy")
   plt.plot(vcpp, "-ro", label = "C++")
   plt.plot(vcppref, "--ro", label = "C++ (n = %d)" % n)
   plt.plot(vf90, "-co", label = "Fortran90")
   plt.xlabel("i")
   plt.ylabel("Mean")
   plt.legend(loc = "best")
   plt.show()

if __name__ == "__main__": main()
