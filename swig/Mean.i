%module Mean

%{
  #define SWIG_FILE_WITH_INIT
  #include "Mean.hxx"
%}

%include "numpy.i"

%init %{
  import_array();
%}

%apply (double* ARGOUT_ARRAY1, int DIM1) {(double* v, int m)};

%include "Mean.hxx"
