! Calculate the mean of a uniform distribution:
subroutine get_mean(n, x, dx, mean)
   
   ! C-binding parameters:
   use iso_c_binding, only: c_int, c_double
   
   ! Arguments:
   integer(c_int), intent(in) :: n
   real(c_double), intent(in) :: x
   real(c_double), intent(in) :: dx
   real(c_double), intent(out) :: mean
   
   ! Internal variables:
   integer :: i
   
   ! Initialize the random seed:
   call srand(time())
   
   ! Calculate the mean:
   mean = 0.0
   do i = 1, n
      mean = mean + x + (-1.0+2.0*rand())*dx
   end do
   mean = mean / n
   
end subroutine get_mean

! Get the evolution of the mean for a uniform distribution:
subroutine get_mean_evol(n, x, dx, v, m)
   
   ! C-binding parameters:
   use iso_c_binding, only: c_int, c_double
   
   ! Arguments:
   integer(c_int), intent(in) :: n
   real(c_double), intent(in) :: x
   real(c_double), intent(in) :: dx
   real(c_double), intent(out) :: v(m)
   integer(c_int), intent(in) :: m
   
   ! Internal variables:
   real(c_double) :: mean
   integer :: i, j
   
   ! Initialize the random seed:
   call srand(time())
   
   ! Calculate the mean:
   mean = 0.0
   do i = 1, m
      do j = 1, n/m
         mean = mean + x + (-1.0+2.0*rand())*dx
      end do
      v(i) = mean / (i*n/m)
   end do
   
end subroutine get_mean_evol
