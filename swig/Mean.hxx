#include "get_mean.h"

#include <cstdlib>
#include <ctime>
#include <stdexcept>

/* The MeanCalculator class: */
class MeanCalculator {
   
   private:
      
      /* Number of points to sample: */
      int n;
      
      /* Mean and half-lenght of the uniform distribution: */
      double x, dx;
   
   public:
      
      /* The MeanCalculator constructor: */
      MeanCalculator(int n, double x, double dx);
      
      /* The MeanCalculator destructor: */
      ~MeanCalculator();
      
      /* Set the calculation parameters: */
      void setParams(int n0, double x0, double dx0);
      
      /* Calculate the mean of a uniform distribution in C++: */
      double getMeanCpp();
      
      /* Calculate the mean of a uniform distribution in Fortran90: */
      double getMeanF90();
      
      /* Get the evolution of the mean for a uniform distribution in C++: */
      void getMeanEvolCpp(double* v, int m);
      
      /* Get the evolution of the mean for a uniform distribution in Fortran90: */
      void getMeanEvolF90(double* v, int m);
   
};
