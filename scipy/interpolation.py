import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import scipy.interpolate as intp

# Plot a surface:
def plotSurf(xx, yy, z, filename):
   
   # Plot the surface:
   fig = plt.figure()
   ax = fig.gca(projection = "3d")
   surf = ax.plot_surface(xx, yy, z, cmap = cm.coolwarm, linewidth = 0, antialiased = False)
   
   # Format the x-axis:
   ax.set_xlim(-1.1, 1.1)
   plt.xticks(np.arange(-1.0, 1.1, step = 0.5))
   ax.xaxis.set_major_formatter(FormatStrFormatter("%.01f"))
   
   # Format the y-axis:
   ax.set_ylim(-1.1, 1.1)
   plt.yticks(np.arange(-1.0, 1.1, step = 0.5))
   ax.yaxis.set_major_formatter(FormatStrFormatter("%.01f"))
   
   # Format the z-axis:
   ax.set_zlim(-0.1, 1.1)
   ax.set_zticks(np.arange(0.25, 1.0, step = 0.25))
   ax.zaxis.set_major_formatter(FormatStrFormatter("%.02f"))
   
   # Format the colorbar:
   fig.colorbar(surf, ax = ax, shrink = 0.5, aspect = 5, ticks = np.arange(0.25, 1.0, step = 0.25))
   
   # Save the figure to a file instead of showing it:
   plt.savefig(filename)

# Main program:
def main():
   
   # Construct a 2D grid:
   x = np.arange(-1.0, 1.001, 0.01)
   y = np.arange(-1.0, 1.001, 0.01)
   xx, yy = np.meshgrid(x, y)
   
   # Create a 2D sine function:
   z = np.sin(xx**2+yy**2)
   
   # Plot the surface:
   plotSurf(xx, yy, z, "surf.png")
   
   # Create an interpolator:
   f = intp.interp2d(x, y, z, kind = "cubic")
   
   # Construct two refined axis:
   xref = np.arange(-1.0, 1.0001, 0.001)
   yref = np.arange(-1.0, 1.0001, 0.001)
   xxref, yyref = np.meshgrid(xref, yref)
   
   # Get the sine function interpolated to the new grid:
   zref = f(xref, yref)
   
   # Plot the surface:
   plotSurf(xxref, yyref, zref, "surfref.png")

if __name__ == "__main__": main()
