from numpy import sqrt, sin, cos, pi
import scipy.integrate as intg
import scipy.special as sp

# Main program:
def main():
   
   # Integrate numerically a bessel function J2.5(x) in the interval [0, 4.5]:
   i = intg.quad(lambda x: sp.jv(2.5, x), 0, 4.5)
   print("Numerical integral: %.16f p/m %.16f" % (i[0], i[1]))
   
   # Calculate the integral analitically:
   i0 = sqrt(2/pi) * (18.0/27*sqrt(2)*cos(4.5) - 4.0/27*sqrt(2)*sin(4.5) + 
      sqrt(2*pi)*sp.fresnel(3/sqrt(pi))[0])
   print("Analytical integral: %.16f" % i0)
   
   # Calculate the numerical error:
   print("Difference: %.16f" % abs(i[0]-i0))

if __name__ == "__main__": main()
