import numpy as np
from scipy import linalg

# Main program:
def main():
   
   # Find the inverse of a random matrix:
   A = np.random.rand(16).reshape(4, 4)
   B = linalg.inv(A)
   I = A.dot(B)
   print("I = ")
   print(I)
   print("det(I) = %.12f" % linalg.det(I))
   print("")
   
   # Solve a linear system of equations:
   A = np.random.rand(25).reshape(5, 5)
   b = np.transpose(np.random.rand(5))
   x = linalg.solve(A, b)
   x0 = linalg.inv(A).dot(b)
   
   # Get the error between the two solutions:
   e1 = linalg.norm(x-x0, 1)
   e2 = linalg.norm(x-x0, 2)
   einf = linalg.norm(x-x0, np.inf)
   print(e1, e2, einf)
   print("")
   
   # Get eigenvalues and eigenvectors:
   A = np.random.rand(9).reshape(3, 3)
   l, v = linalg.eig(A)
   print("l = ")
   print(l)
   print("v = ")
   print(v)

if __name__ == "__main__": main()
