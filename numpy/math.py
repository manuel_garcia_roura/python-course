import numpy as np

# Main program:
def main():
   
   # Create some random arrays:
   v1 = np.random.rand(8).reshape(2, 4)
   v2 = np.random.rand(8).reshape(2, 4)
   
   # Do some vector math:
   v3 = v1 + v2 # Element-wise sum.
   v4 = v1 * v2 # Element-wise product.
   v5 = v1.dot(np.transpose(v2)) # Dot product.
   v2 *= np.pi # Scalar-vector product (in-place).
   
   # Evaluate a function in an array:
   v6 = np.sqrt(v2)
   v7 = np.sin(2.0*np.pi*v1)
   
   # Print some arrays:
   print("v1 = ")
   print(v1)
   print("v2 = ")
   print(v2)
   print("v5 = ")
   print(v5)
   print("v7 = ")
   print(v7)

if __name__ == "__main__": main()
