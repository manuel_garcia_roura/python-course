import numpy as np

# Main program:
def main():
   
   # Create some arrays from lists:
   v1 = np.array([1, 1, 1, 1, 2, 2, 2, 2])
   v2 = np.array([[1, 1, 1, 1], [2, 2, 2, 2]])
   
   # Create some arrays with built-in functions:
   v3 = np.arange(8).reshape(2, 4)
   v4 = np.zeros(8).reshape(2, 4)
   v5 = np.ones(8).reshape(2, 4)
   v6 = np.empty(8).reshape(2, 4)
   v7 = np.random.rand(8).reshape(2, 4)
   
   # Print some arrays:
   print("v1 = ")
   print(v1)
   print("v2 = ")
   print(v2)
   print("v4 = ")
   print(v4)
   print("")
   
   # Print a sliced array:
   print("v3 = ")
   print(v3)
   print("v3[1, 1:3] = ")
   print(v3[1, 1:3])
   print("")
   
   # Print a flattened array:
   print("v6 = ")
   print(v6)
   print("v6.ravel() = ")
   print(v6.ravel())
   print("")
   
   # Print some attributes:
   print("v1", v1.shape, v1.ndim, v1.dtype.name, v1.itemsize, v1.size)
   print("v5", v5.shape, v5.ndim, v5.dtype.name, v5.itemsize, v5.size)
   print("v7", v7.shape, v7.ndim, v7.dtype.name, v7.itemsize, v7.size)

if __name__ == "__main__": main()
