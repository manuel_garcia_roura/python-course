import numpy as np

import time as t

# Main program:
def main():
   
   # Create a big array:
   n = 10000000
   v = np.random.rand(n)
   
   # Get the sum of all elements with normal Python:
   tpy = t.time()
   sumpy = 0.0
   for i in range(n):
      sumpy += v[i]
   tpy = t.time() - tpy
   
   # Get the sum of all elements with NumPy:
   tnp = t.time()
   sumnp = v.sum()
   tnp = t.time() - tnp
   
   # Print the main results:
   print("Python: %f - %f" % (sumpy, tpy))
   print("NumPy: %f - %f (%f%%)" % (sumnp, tnp, (tnp/tpy)*100))

if __name__ == "__main__": main()
