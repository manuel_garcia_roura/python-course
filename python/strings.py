# Main program:
def main():
   
   # Print some stuff using formatting:
   k = 1.0
   print("")
   s = "A reactor is %s if k = %0.3f, \nsubcritical if k < %0.3f, etc..." % ("critical", k, k)
   print(s)
   print("")
   
   # Some sting methods:
   n1 = s.count("k")
   n1 = s.count("$")
   if s.endswith("..."):
      lazy = True
   else:
      lazy = False
   if lazy:
      s += " (TODO: complete the rule)"
   print(s)
   print("")
   
   # Some more methods:
   s = s.replace("a", "x")
   for word in s.split(" "):
      print(word)
   print("")

if __name__ == "__main__": main()
