import Workers as w

# Main program:
def main():
   
   # Check what a worker does:
   worker = w.Worker()
   print("")
   print("The generic worker needs %s." % worker.needs().lower())
   print("The generic worker produced %s." % worker.work().lower())
   print("")
   
   # Check what a carpenter does:
   carpenter = w.Carpenter()
   print("The carpenter needs %s." % carpenter.needs().lower())
   print("The carpenter produced %s." % carpenter.work().lower())
   print("Can the carpenter come to my house? %s." % carpenter.askToComeToMyHouse())
   print("")
   
   # Check what a scientist does:
   scientist = w.Scientist()
   print("The scientist needs %s." % scientist.needs().lower())
   print("The scientist produced %s." % scientist.work().lower())
   print("The scientist said '%s' when I didn't give him money." % scientist.makeSomethingUp(False))
   print("The scientist said '%s' when I gave him money.." % scientist.makeSomethingUp(True))
   print("")

if __name__ == "__main__": main()
