import random as r

# Get a random float in a range:
def get_float(x1, x2):
   "This function creates a random float in a given range."
   
   # Just return a random number:
   return r.uniform(x1, x2)

# Print a float:
def print_float(x):
   "This function prints a float to the standard output."
   
   # Print the float:
   print("")
   print("x = %0.5f" % x)

# Pick a float in an array:
def pick_float(v):
   "This function picks a random float from a given array."
   
   # Choose a number if the argument is a list or tuple:
   if isinstance(v, (list, tuple)):
      x = r.choice(v)
      v.remove(x)
      return x, True
   else:
      return v, False

# Main program:
def main():
   
   # Print some random float:
   print_float(get_float(-1.0, 1.0))
   
   # Pick a random float from a list:
   v = [r.random() for i in range(10)]
   x, flag = pick_float(v)
   print("")
   print(x)
   print(flag)
   print(len(v))
   print("")
   x = pick_float(v)
   print(x)
   print(len(v))
   print("")

if __name__ == "__main__": main()
