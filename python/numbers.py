import math as m
import random as r

# Main program:
def main():
   
   # Some numbers (the type is automatic):
   x = 1.0
   n = 5
   h = 10 # Can we name it m?
   
   # Some math:
   y = (n/h)*x
   z = (float(n)/h)*x
   print("")
   print(y, z, complex(z), min(x, y, z))
   print(m.cos(m.pi*x), m.sin(m.pi*y), m.tan(m.pi*z))
   print("")
   
   # Some functions for random stuff:
   l = [3, "RPD", [0.1, 0.2, 0.3]]
   for i in range(10):
      print(r.choice(l))
   print("")
   for i in range(10):
      print(r.random())
   print("")
   for i in range(10):
      print(r.uniform(-3.0, 3.0))
   print("")

if __name__ == "__main__": main()
