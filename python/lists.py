# Main program:
def main():
   
   # Get some foods:
   german = ["Schnitzel", "'Diavolo' Pizza", "Flammkuchen", "Kebab"]
   italian = ["Lasagna", "Ravioli"]
   argentinean = ["Asado", "Empanadas"]
   more = ["Fisch", "Agnolotti", "Maultaschen"]
   
   # Organize the foods:
   while more:
      food = more.pop()
      if "sch" in food.lower():
         german.append(food)
      elif "otti" in food.lower():
         italian.append(food)
      else:
         print("Warning! Origin unknown!")
      
   # Get the tasty foods:
   tasty = []
   for food in german + italian + argentinean:
      if (food in italian) or (food in argentinean):
         tasty.append(food)
      if food in german:
         if food != "'Diavolo' Pizza" and food != "Maultaschen":
            tasty.append(food)
   
   # Print the three winners:
   tasty.sort()
   tasty.reverse()
   tasty.remove("Asado")
   print(tasty[0:3])

if __name__ == "__main__": main()
