import os
import re

# Main program:
def main():
   
   # Write some stuff to a file:
   fout = open("data.txt", "w")
   for i in range(10):
     fout.write("This is line %d\n" % (i+1))
   fout.close()
   
   # Read from the file line by line:
   fin = open("data.txt", "r")
   content = fin.readlines()
   print("")
   for line in content:
      if "5" in line:
         print(line.strip())
   print("")
   fin.close()
   
   # Read from the file using regular expresions:
   fin = open("data.txt", "r")
   content = fin.read()
   ints = re.findall(r"\d+", content)
   ints = [int(i) for i in ints]
   print(ints)
   l5 = re.findall(r" \S+ 5", content)
   print(l5)
   print("")
   fin.close()
   
   # Remove the file:
   os.remove("data.txt")

if __name__ == "__main__": main()
