#!/usr/bin/python

"""
Multi-line comments are used for:
   Function and class documentation.
   Multi-line comments (?).
"""

# Print some greetings:
# You can skip the parentheses.
print("")
print("Hello, Python!")
print("Python is:\n  Interpreted\n  Interactive\n  Object-oriented\n  Easy to learn (?)")
print("")

# Create some variables (no need to declare them explicitly):
# The names are case-sensitive, with rules similar to other languages.
# There are some reserved words (usually highlighted by decent text editors).
n = 0
x = 0.0
x = 1.0 + 1.0j
s = "INR"
s += "-RPD"
print(s)
print("")

# Create some lists:
l1 = [1, 2, 3]
l2 = [3, "RPD", 
   [0.1, 0.2, 0.3]]
l3 = l1 + l2
print(l3)
print("")

# Create some tupples (read-only lists):
t1 = (10, ["a", "b"])
t1[1][1] = "c"
t2 = (1, 2, 3)
t3 = t1 + t2
print(t3)
print(list(t3))
print("")

# Create a dictionary:
d = {"German foods": ["Schnitzel", "'Diavolo' Pizza"], "German drinks": ['Bier', '%d Biere' % 3]}
print(d["German foods"])
print(d["German drinks"])
print("")

# Indentation is mandatory and determines the logic:
if 2+2 == 4:
   print("2+2 = 4")
else:
   print("2+2 != 4")
print("")
