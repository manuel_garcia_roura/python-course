import random as r
import bisect as b

# A generic worker:
class Worker:
   
   # The Worker constructor:
   def __init__(self):
      
      # Tools:
      self.tools = ["Nothing"]
      
      # Products:
      self.products = [("Nothing", 1.0)]
   
   # Check what the worker needs:
   def needs(self):
      
      # Return a random tool:
      return r.choice(self.tools)
   
   # Do some work:
   def work(self):
      
      # Choose a random product:
      weights = [item[1] for item in self.products]
      x = r.random()
      idx = b.bisect(weights, x)
      product = self.products[idx][0]
      
      # Return some product:
      return product

# A carpenter:
class Carpenter(Worker):
   
   # The Carpenter constructor:
   def __init__(self):
      
      # Tools:
      self.tools = ["A hammer", "A pencil", "A saw", "Some nails"]
      
      # Products:
      self.products = [("A chair", 0.4), ("A table", 0.7), ("A bed", 1.0)]
   
   # Ask the carpenter to come to my house:
   def askToComeToMyHouse(self):
      
      # Return yes or no:
      return r.choice(["Yes", "No"])

# A scientist:
class Scientist(Worker):
   
   # The Scientist constructor:
   def __init__(self):
      
      # Tools:
      self.tools = ["A computer", "A coffee", "A doughnuts", "More time", "More money"]
      
      # Products:
      self.products = [("A paper", 0.1), ("A theorem", 0.2), ("Nothing", 1.0)]
   
   # Ask the scientist to make something up:
   def makeSomethingUp(self, more_funding):
      
      # Make something up:
      if more_funding:
         syms = ["alpha", "beta", "gamma"]
         vals = [-1.0, 0.0, 1.0]
         theory = r.choice(syms) + " = " + str(r.choice(vals))
      else:
         theory = "I need more money"
      
      # Return the theory:
      return theory
