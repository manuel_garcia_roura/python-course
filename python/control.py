# Main program:
def main():
   
   # A one-liner if:
   n = 1
   print("")
   if n == 1: print("n = 1"); print("Good!")
   print("")
   
   # A multi-line if:
   x = 1.0
   if x > 0.0:
      print("x > 0.0")
   else:
      print("x < 0.0")
   print("")
   
   # A normal for loop:
   n = 10
   s = 0
   for i in range(n):
      s += i
   print(s)
   print(range(n))
   print("")
   
   # Some strange for loops:
   l = [3, "RPD", [0.1, 0.2, 0.3]]
   for x in l:
      print(x)
   for c in l[1]:
      print(c)
   for x in l[2]:
      print(x)
   print("")

if __name__ == "__main__": main()
