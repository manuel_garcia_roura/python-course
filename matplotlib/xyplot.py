import numpy as np
import matplotlib.pyplot as plt

# Main program:
def main():
   
   # Create the axis:
   x = np.linspace(0.0, 2.0, 100)
   
   # Plot some functions:
   plt.plot(x, x, label = "y = x")
   plt.plot(x, x**2, label = "y = x**2")
   plt.plot(x, x**3, label = "y = x**3")
   
   # Add labels:
   plt.xlabel("x")
   plt.ylabel("y")
   plt.title("Some y = f(x) functions")
   plt.legend()
   
   # Show the plot:
   plt.show()

if __name__ == "__main__": main()
