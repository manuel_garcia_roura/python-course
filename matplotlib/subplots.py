import numpy as np
import matplotlib.pyplot as plt

# Main program:
def main():
   
   # Create a couple of axes:
   x1 = np.linspace(0.0, 5.0)
   x2 = np.linspace(0.0, 2.0)
   
   # Create some functions in these axes:
   y1 = np.cos(2*np.pi*x1) * np.exp(-x1)
   y2 = np.cos(2*np.pi*x2)
   
   # Plot the first function:
   plt.subplot(2, 1, 1)
   plt.plot(x1, y1, "o-")
   plt.ylabel("y (damped)")
   
   # Plot the second function:
   plt.subplot(2, 1, 2)
   plt.plot(x2, y2, ".-")
   plt.ylabel("y (undamped)")
   plt.xlabel("t (s)")
   
   # Show the plot:
   plt.show()

if __name__ == "__main__": main()
