import numpy as np
import matplotlib.pyplot as plt

# Main program:
def main():
   
   # Build a vector of 10000 normal samples with standard deviation 0.5 and mean 2.0:
   mu, sigma = 2.0, 0.5
   v = np.random.normal(mu, sigma, 10000)
   
   # Plot a normalized histogram with 50 bins:
   plt.hist(v, bins = 50)
   
   # Add labels:
   plt.xlabel("x")
   plt.ylabel("p")
   plt.title("A p(x) histogram")
   plt.legend()
   
   # Show the plot:
   plt.show()

if __name__ == "__main__": main()
