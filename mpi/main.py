from mpi4py import MPI
import numpy as np

# Get the communicator and the rank:
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
print("Hello from rank %d/%d" % (rank, size))

# Exchange Python objects:
if rank == 0:
   data = {'a': 7, 'b': 3.14}
   comm.send(data, dest = 1, tag = 11)
elif rank == 1:
   data = comm.recv(source = 0, tag = 11)

# Exchange Python objects with non-blocking communication:
if rank == 0:
   data = {'a': 7, 'b': 3.14}
   req = comm.isend(data, dest = 1, tag = 11)
   req.wait()
elif rank == 1:
   req = comm.irecv(source = 0, tag = 11)
   data = req.wait()

# NumPy arrays:

# Passing MPI datatypes explicitly:
if rank == 0:
   data = np.arange(1000, dtype = 'i')
   comm.Send([data, MPI.INT], dest = 1, tag = 77)
elif rank == 1:
   data = np.empty(1000, dtype = 'i')
   comm.Recv([data, MPI.INT], source = 0, tag = 77)

# Automatic MPI datatype discovery:
if rank == 0:
   data = np.arange(100, dtype = np.float64)
   comm.Send(data, dest = 1, tag = 13)
elif rank == 1:
   data = np.empty(100, dtype = np.float64)
   comm.Recv(data, source = 0, tag = 13)

# Broadcast a NumPy array:
if rank == 0:
   data = np.arange(100, dtype = 'i')
else:
   data = np.empty(100, dtype = 'i')
comm.Bcast(data, root = 0)
for i in range(100):
   assert data[i] == i
